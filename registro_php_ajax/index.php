<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="jquery-3.3.1.min.js"></script>

</head>
<body>
    <form action="" method="POST" id="formulario">
        <label for="">Ingrese nombres:</label>
        <input type="text" name="nombre" id="nombre" placeholder="Ingrese sus nombres">
        <label for="">Ingrese apellidos:</label>
        <input type="text" name="apellido" id="apellidos" placeholder="Ingrese sus apellidos">
        <label for="">Ingrese usuario:</label>
        <input type="text" name="usuario" id="usuario" placeholder="Ingrese su usuario">
        <label for="">Ingrese contraseña:</label>
        <input type="password" name="password" id="contraseña" placeholder="***********">
        <label for="">Ingrese Correo:</label>
        <input type="text" name="correo" id="correo" placeholder="example@gmail.com">
        <input type="submit" value="Registrar" name="Boton" id="Boton">
    </form>
    
</body>
</html>

<script>
$(document).ready(function() {
    $('#Boton').click(function() {
        var datos = $('#formulario').serialize();
            $.ajax({
                type: "POST",
                url: "insertar.php",
                data: datos,
                success:function(r) {
                    if (r==1) {
                        alert('agregado con exito');
                    }else{
                        alert('fallo el servidor');
                    }
                }
            });
        return false;
    });
});
</script>