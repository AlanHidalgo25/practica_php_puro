<?php
if (!$_POST) {
    header("location:conversor_monedas.php");
}

function Convertir()
{
    $numero = $_POST['numero'];
    $moneda = $_POST['moneda'];

    if ($moneda == 'usd') {
        $precio = 3.00;
    }else {
        $precio = 4.00;
    }

    $total = $numero*$precio;
    return $total;
}

$resultado = Convertir();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <?php
echo "el resultado es: " .   $resultado;  
?>

</body>
</html>