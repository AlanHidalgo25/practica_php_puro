<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Funciones Definidas:</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>
<body>
<?php

/*function Conversor($cantidad)
{
    $total = $cantidad*2000000;
    echo ("la cantidad de " . $cantidad . " en dolares es: " . $total);
}

Conversor(10);
*/

function Suma($a, $b)
{
    $total = $a+$b;
    return $total;
}

$resultado = Suma(2,2);
echo "El resultado es: " . $resultado;

?>
    
</body>
</html>