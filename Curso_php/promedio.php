<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>
<body>
    <h1>Calcular promedio:</h1>
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="POST">
        <label for="">Ingrese Nombres:</label>
        <input type="text" name="nombres">
        <label for="">Ingrese nota number 1:</label>
        <input type="text" name="nota1">
        <label for="">Ingrese nota number 2::</label>
        <input type="text" name="nota2">
        <label for="">Ingrese nota number 3:</label>
        <input type="text" name="nota3">
        <input type="submit" value="Calcular" name="calcular">
    </form>
    
</body>
</html>

<?php
if (isset($_POST['calcular'])) {
    $nombres = $_POST['nombres'];
    $nota1 = $_POST['nota1'];
    $nota2 = $_POST['nota2'];
    $nota3 = $_POST['nota3'];
    $promedio = ($nota1+$nota2+$nota3)/3;
    echo($nombres . " Tu promedio es de: " . $promedio);
}

?>