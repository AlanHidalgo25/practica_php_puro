<?php

if (isset($_POST)) {
    header("location: envio_de_datos.php");
}

$producto = $_POST['producto'];
$precio = $_POST['precio'];
$cantidad = $_POST['cantidad'];
$subtotal = $_POST['precio'] * $_POST['cantidad'];
$forma_pago = $_POST['forma_pago'];

if ($forma_pago == 'tarjeta') {
    $descuento = ($subtotal * 0.2);
}else {
    $descuento = ($subtotal * 0.1);
}
$total = $subtotal - $descuento;

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Boleta</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>
<body>

<h1>Total a Pagar</h1>

<p>Producto: <?php echo $producto ?></p>
<p>Precio: <?php echo $precio ?></p>
<p>Cantidad: <?php echo $cantidad ?></p>  
<p>descuento: <?php echo $descuento ?></p> 
<p>Forma de pago: <?php echo $forma_pago ?></p>
<p>Subtotal: <?php echo $subtotal ?></p>  
<p>Total: <?php echo $total ?></p>  
</body>
</html>