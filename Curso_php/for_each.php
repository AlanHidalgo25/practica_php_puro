<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>For Each</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <h1>ForEach</h1>
    
</body>
</html>

<?php
/* 
FUNCIONES:
*sort() -> Ordena un Array.
*rsort() -> Devuelve un Array en modo inverso
*asort() -> Ordena un Array y mantiene la asociacion de indices.
*arsort() -> Ordena un Array en orden inverso y mantiene la asociacion de indices.
*count() -> Cuentas los elementos de un Array.
*/

$contenidos = ['Alan', 'Angelito', 'Nelly', 'Florentino'];
rsort($contenidos)
foreach ($contenidos as $contenido => $value) {
    echo $value . "</br>";
}

?>