<!DOCTYPE html>
<html>
<head>
	<title>Formulario de registro</title>
	<link rel="stylesheet" type="text/css" href="estilos.css">
</head>
<body>

<h1>Formulario de registro</h1>
<form name="formulario" method="post" action="registrar.php" class="form_register" onsubmit="return validar();">
	<h2 class="form_titulo">CREA UNA CUENTA</h2>
	<div class="contenedor-inputs">
		<input type="text" id="nombre" name="nombre" placeholder="Nombre" class="input-48">
		<input type="text" id="apellidos" name="apellidos" placeholder="Apellidos" class="input-48">
		<input type="email" id="email" name="correo" placeholder="Correo" class="input-100">
		<input type="text" id="usuario" name="usuario"
		placeholder="Usuario" class="input-48">
		<input type="password" id="contraseña" name="clave" placeholder="Contraseña" class="input-48">
		<input type="text" id="telefono" name="telefono"
		placeholder="Telefono" class="input-100">
		<input type="submit" name="registrar" value="Registrar" class="btn-enviar">
		<p>¿Ya tienes una cuenta? <a href="#">Ingresa aqui</a></p>
	</div>
	
</form>

</body>
</html>

