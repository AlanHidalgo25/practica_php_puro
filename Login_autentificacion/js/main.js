$(document).on('submit', '#formlg', function (event) {
    event.preventDefault();

    $.ajax({
        type: "POST",
        url: 'login.php',
        data: $(this).serialize(),
        dataType: "json",
        beforeSend: function () {
            $('.botonlg').val('Validando...');
        }
    })
    .done(function (respuesta) {
        console.log(respuesta);
        if (!respuesta.error) {
            if (respuesta.tipo = 'Administrador') {
                location.href = 'Admin';

            }else if (respuesta.tipo = 'Usuario') {
                location.href = 'Usuario';
            }
        }else {
            $('.error').slideDown('slow');
            setTimeout(function() {
                $('.error').slideUp('slow');
            }, 2000);
            $('.botonlg').val('Iniciar Sesion');
        }
    })
    .fail(function (resp) {
        console.log(resp.responseText);
    })
    .always(function () {
        console.log("complete");
    });
});
