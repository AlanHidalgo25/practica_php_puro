<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login y autentificacion:</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
</head>
<body>
    <div class="error">
        <span> Datos de ingreso no validos, por favor intentar de nuevo. </span>
    </div>

    <div class="main">
        <form action= "" id="formlg">
            <input type="text" name="usuariolg" placeholder="Usuario" required />
            <input type="password" name="passlg" placeholder="Contraseña" required />
            <input type="submit" name="botonlg" class="botonlg" value="Iniciar Sesion" />
        </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>

  <script src="js/main.js"></script>
    
</body>
</html>